const PluginCommunicationServer = require('./PluginCommunication/PluginCommunicationServer');
const AppCommunicationServer = require('./AppCommunication/AppCommunicationServer');

PluginCommunicationServer.start();
AppCommunicationServer.start();