const UUIDV4 = require('uuid/v4');
const fs = require('fs');

module.exports = class {
    static saveFromBase64(base64) {
        let outputFileExtension = '.jpg';
        if (base64.match('image\/png')) {
            outputFileExtension = '.png';
        }

        let base64Data = base64.replace(/^data:image(?:.*);base64,/, "");

        let directory = 'images/' + UUIDV4() + '/';

        fs.mkdirSync('web/' + directory, { recursive: true });

        let filename = directory + UUIDV4() + outputFileExtension;
        fs.writeFileSync('web/' + filename, base64Data, 'base64');

        return filename;
    }
};