const ChallengeForwarder = require("../AntiCheat/ChallengeForwarder");
const WebsocketServer = require('websocket').server;
const Https = require('https');
const Config = require('../config');
const fs = require('fs');

module.exports = class {
    static start() {
        let server = Https.createServer({
                key: fs.readFileSync(Config.plugin_communication.ssl_key_location),
                cert: fs.readFileSync(Config.plugin_communication.ssl_certificate_location)
            },
            function(request, response) {
            console.log((new Date()) + ' Received request for ' + request.url);
            response.writeHead(404);
            response.end();
        });

        server.listen(Config.plugin_communication.port, function() {
            console.log((new Date()) + ' Server is listening on port 1798');
        });

        let wsServer = new WebsocketServer({
            httpServer: server,
            // You should not use autoAcceptConnections for production
            // applications, as it defeats all standard cross-origin protection
            // facilities built into the protocol and the browser.  You should
            // *always* verify the connection's origin and decide whether or not
            // to accept it.
            autoAcceptConnections: false
        });

        function originIsAllowed(origin) {
            //TODO only allow connections from cheat Chrome plugin
            console.log(origin);
            return true;
        }

        wsServer.on('request', function(request) {
            console.log('request');
            if (!originIsAllowed(request.origin)) {
                // Make sure we only accept requests from an allowed origin
                request.reject();
                console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
                return;
            }

            let connection = request.accept('chrome-extension-v1', request.origin);

            console.log((new Date()) + ' Connection accepted.');
            connection.on('message', function(message) {
                if (message.type === 'utf8') {
                    console.log('Received Message: ' + message.utf8Data);

                    ChallengeForwarder.sendToCompanionApp(message.utf8Data, connection);
                }
                else if (message.type === 'binary') {
                    console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                    connection.sendBytes(message.binaryData);
                }
            });
            connection.on('close', function(reasonCode, description) {
                console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
            });
        });
    }

    static sendChallengeResponse(connection, uuid, answer) {
        connection.send({
            type: "challengeResponse",
            uuid: uuid,
            answer: answer
        })
    }
};