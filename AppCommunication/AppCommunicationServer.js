const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const Config = require('../config');
const querystring = require('querystring');
const ChallengeForwarder = require('../AntiCheat/ChallengeForwarder');

module.exports = class {
    static start() {
        // maps file extention to MIME types
        const mimeType = {
            '.ico': 'image/x-icon',
            '.html': 'text/html',
            '.js': 'text/javascript',
            '.json': 'application/json',
            '.css': 'text/css',
            '.png': 'image/png',
            '.jpg': 'image/jpeg',
            '.wav': 'audio/wav',
            '.mp3': 'audio/mpeg',
            '.svg': 'image/svg+xml',
            '.pdf': 'application/pdf',
            '.doc': 'application/msword',
            '.eot': 'appliaction/vnd.ms-fontobject',
            '.ttf': 'aplication/font-sfnt'
        };

        http.createServer(function (req, res) {
            console.log(`${req.method} ${req.url}`);

            // parse URL
            let parsedUrl = url.parse(req.url);

            let matches = parsedUrl.pathname.match(/^\/challenge\/(.*)\/solve$/);
            if (matches !== null && matches.length > 0 && req.method === 'POST') {
                let uuid = matches[1];

                let body = '';
                req.on('data', chunk => {
                    body += chunk.toString(); // convert Buffer to string
                });
                req.on('end', () => {
                    let data = querystring.parse(body);
                    ChallengeForwarder.forwardChallengeAnswer(uuid, data['answer']);
                });
            }

            // extract URL path
            // Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
            // e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
            // by limiting the path to current directory only
            let sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');

            let pathname = path.join(__dirname + '/../web', sanitizePath);
            fs.exists(pathname, function (exist) {
                if(!exist) {
                    // if the file is not found, return 404
                    res.statusCode = 404;
                    res.end(`File ${pathname} not found!`);
                    return;
                }

                // if is a directory, then look for index.html
                if (fs.statSync(pathname).isDirectory()) {
                    pathname += '/index.html';
                }

                // read file from file system
                fs.readFile(pathname, function(err, data){
                    if(err){
                        res.statusCode = 500;
                        res.end(`Error getting the file: ${err}.`);
                    } else {
                        // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                        const ext = path.parse(pathname).ext;
                        // if the file is found, set Content-type and send data
                        res.setHeader('Content-type', mimeType[ext] || 'text/plain' );
                        res.end(data);
                    }
                });
            });


        }).listen(Config.app_communication.port);

        console.log(`Server listening on port ${Config.app_communication.port}`);
    }
};