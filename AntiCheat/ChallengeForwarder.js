'use strict';

const Config = require('../config.json');
const FCM = require('fcm-node');
const FCMServerKey = require('../var/camorra-world-cheat-firebase-adminsdk');
const ImageHandler = require('../ImageHandler/ImageHandler');
const PluginCommunicationServer = require('../PluginCommunication/PluginCommunicationServer');

const fcm = new FCM(FCMServerKey);

module.exports = class {
    static sendToCompanionApp(challenge, connection) {
        let parsedChallenge = JSON.parse(challenge);

        let imageLocation = null;

        if (typeof (parsedChallenge.image) !== 'undefined') {
            imageLocation = ImageHandler.saveFromBase64(parsedChallenge.image);
        }

        let data = {};
        if (parsedChallenge['type'] === "objectRecognition") {
            // Copy all but choices
            for (let key in parsedChallenge) {
                if (!parsedChallenge.hasOwnProperty(key)) {
                    continue;
                }

                if (key === 'choices') {
                    continue;
                }

                data[key] = parsedChallenge[key];
            }

            // Flatten choices into their own properties
            for (let i in parsedChallenge['choices']) {
                if (!parsedChallenge['choices'].hasOwnProperty(i)) {
                    continue;
                }

                data['choice_' + i] = parsedChallenge['choices'][i];
            }
        } else {
            data = parsedChallenge;
        }

        if (imageLocation !== null) {
            data['image'] = Config.app_communication.hostname + ':' + Config.app_communication.port + '/' + imageLocation;
        }

        data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';

        let message = {
            to: Config.fcm.registration_token,

            notification: {
                title: 'AntiCheat is actief',
                body: 'Bekijk in de app'
            },

            data: data
        };

        if (typeof this.challenges === "undefined") {
            this.challenges = [];
        }

        this.challenges[data['uuid']] = connection;

        fcm.send(message, function(err, response) {
            if (err) {
                console.log("Something has gone wrong!", err);
            } else {
                console.log("Successfully sent with response: ", response);
                console.log(response.results[0].error);
            }
        });
    }

    static forwardChallengeAnswer(uuid, answer) {
        let connection = this.challenges[uuid];

        PluginCommunicationServer.sendChallengeResponse(connection, uuid, answer);
    }
};