#!/usr/bin/env bash

node > ./output_config.json <<EOF
// Read data
var data = require('./config.json');

data.fcm.registration_token = "$FCM_REGISTRATION_TOKEN"
data.app_communication.hostname = "$APP_COMMUNICATION_HOSTNAME"

data.plugin_communication.ssl_key_location = "$PLUGIN_COMMUNICATION_SSL_KEY_LOCATION"
data.plugin_communication.ssl_certificate_location = "$PLUGIN_COMMUNICATION_SSL_CERTIFICATE_LOCATION"

//Output data
console.log(JSON.stringify(data));

EOF

cp ./output_config.json ./config.json